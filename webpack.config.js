'use strict';
var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    devtool: 'evel-source-map',
    context: path.resolve(__dirname + '/http'),
    entry: {
        app: './app.js',
        vendor: ['jquery', 'popper.js', 'bootstrap', 'bootstrap/js/dist/util.js', 'bootstrap/js/dist/dropdown.js', 'mapbox-gl', 'angular-mapboxgl-directive'],
        angular: ['angular', 'angular-ui-router']
    },
    output: {
        path: path.resolve(__dirname + '/dist'),
        filename: 'js/[name].bundle.js',
        publicPath: '',
    },
    devServer: {
        historyApiFallback: true,
        inline: true
    },
    module: {
        rules: [
            {
                test: /\.view.html$/,
                use: [
                    {
                        loader: 'ngtemplate-loader'
                    },
                    {
                        loader: 'html-loader'
                    }
                ],
            },
            {
                test: /\.css$/,
                use: [{
                    loader: 'style-loader'
                }, {
                    loader: 'css-loader'
                }]
            },
            {
                test: /\.(scss)$/,
                use: [{
                    loader: 'style-loader',
                }, {
                    loader: 'css-loader',
                }, {
                    loader: 'postcss-loader',
                    options: {
                        plugins: function () {
                            return [
                                require('precss'),
                                require('autoprefixer')
                            ];
                        }
                    }
                }, {
                    loader: 'sass-loader'
                }]
            },
            {
                test: /\.(jpg|png|gif)(\?.*$|$)/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 10000,
                            name: './images/[name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|svg)(\?.*$|$)/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: './fonts/[name].[ext]'
                    }
                }]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: __dirname + '/http/templates/index.tmpl.html',
            chunksSortMode: function (chunk1, chunk2) {
                var order = ['angular', 'vendor', 'app'];
                var order1 = order.indexOf(chunk1.names[0]);
                var order2 = order.indexOf(chunk2.names[0]);
                return order1 - order2;
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({
            names: ['vendor', 'angular'],
            filename: 'js/[name].bundle.js'
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
            Util: 'exports-loader?Util!bootstrap/js/dist/util',
            Dropdown: 'exports-loader?Dropdown!bootstrap/js/dist/dropdown',
            mapboxgl: 'mapbox-gl'
        })
    ]
};