'use strict';
var angular = require('angular');

function views() {
    return {
        demo: require('./demo/demo.view.html'),
        menu: require('./menu/menu.view.html'),
        map: require('./map/map.view.html'),
        about: require('./about/about.view.html')
    };
}

exports = module.exports = views();

var viewsModule = angular.module('wabgApp.views', [])
    .constant(views.name, exports);

exports.default = viewsModule.name;