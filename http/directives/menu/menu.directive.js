'use strict';

function menuDirective(views) {
    return {
        restrict: 'E',
        templateUrl: views.menu
    };
}

exports = module.exports = menuDirective;