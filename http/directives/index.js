'use strict';
var angular = require('angular');

var demoDirective = require('./demo/demo.directive');
var menuDirective = require('./menu/menu.directive');

var directivesModule = angular.module('wabgApp.directives', [])
    .directive(demoDirective.name, ['views', demoDirective])
    .directive(menuDirective.name, ['views', menuDirective]);

exports = module.exports = {
    default: directivesModule.name,
    demoDirective: demoDirective.name,
    menuDirective: menuDirective
};
