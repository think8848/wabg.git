'use strict';
var angular = require('angular');

var defaultMapStyle = require('./map-styles/default-style');

var servicesModule = angular.module('wabgApp.services', [])
    .constant(defaultMapStyle.name, defaultMapStyle());

exports = module.exports = {
    default: servicesModule.name
};