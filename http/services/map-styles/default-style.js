'use strict';

function defaultMapStyle() {
    return {
        'version': 8,
        'sources': {
            'raster-tiles': {
                type: 'raster',
                'tiles': ['http://mt3.google.cn/vt/lyrs=s&hl=zh-CN&gl=cn&x={x}&y={y}&z={z}'],
                'tileSize': 256
            }
        },
        'layers': [{
            'id': 'simple-tiles',
            'type': 'raster',
            'source': 'raster-tiles',
            'minzoom': 0,
            'maxzoom': 22
        }]
    };
}

exports = module.exports = defaultMapStyle;
