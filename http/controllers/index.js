'use strict';
var angular = require('angular');

var homeController = require('./home/home.controller');
var mapController = require('./map/map.controller');
var aboutController = require('./about/about.controller');

var controllersModule = angular.module('wabgApp.controllers', [])
    .controller(homeController.name, ['$scope', homeController])
    .controller(mapController.name, ['$scope', 'defaultMapStyle', mapController])
    .controller(aboutController.name, ['$scope', aboutController]);

exports = module.exports = {
    default: controllersModule.name,
    homeController: homeController.name,
    mapController: mapController.name,
    aboutController: aboutController.name
};