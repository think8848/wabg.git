'use strict';
var angular = require('angular');
var uirouter = require('angular-ui-router');

var css = require('./css/index.css');
var scss = require('./scss/index.scss');

var services = require('./services');
var directives = require('./directives');
var controllers = require('./controllers');
var views = require('./views');

var mapboxglDirective = 'mapboxgl-directive';

angular.module('wabgApp', [uirouter.default, services.default, views.default, directives.default, controllers.default, mapboxglDirective])
    .config(function ($stateProvider) {
        $stateProvider
            .state('home', {

            })
            .state('about', {
                url: '/about',
                controller: controllers.aboutController,
                templateUrl: views.about
            })
            .state('map', {
                url: '/map',
                controller: controllers.mapController,
                templateUrl: views.map
            });
    })
    .run([function () {
        mapboxgl.accessToken = true;
    }]);
